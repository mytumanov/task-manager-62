package ru.mtumanov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryRq extends AbstractUserRq {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    public UserRegistryRq(
            @Nullable final String token,
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        super(token);
        this.login = login;
        this.password = password;
        this.email = email;
    }

}