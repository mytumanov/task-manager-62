package ru.mtumanov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.comparator.CreatedComparator;
import ru.mtumanov.tm.comparator.NameComparator;
import ru.mtumanov.tm.comparator.StatusComparator;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<TaskDTO> comparator;

    TaskSort(@NotNull final String displayName, @NotNull final Comparator<TaskDTO> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static TaskSort toSort(@NotNull final String value) {
        if (value.isEmpty())
            return null;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value))
                return sort;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator<TaskDTO> getComparator() {
        return comparator;
    }

}
