package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.mtumanov.tm.api.repository.dto.IDtoRepository;
import ru.mtumanov.tm.api.service.dto.IDtoService;
import ru.mtumanov.tm.dto.model.AbstractModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

public abstract class DtoAbstractService<M extends AbstractModelDTO, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected R repository;

    @Override
    @NotNull
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public void set(@NotNull final Collection<M> models) throws AbstractException {
        repository.saveAll(models);
    }

}
