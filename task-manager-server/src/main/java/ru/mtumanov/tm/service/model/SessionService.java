package ru.mtumanov.tm.service.model;

import ru.mtumanov.tm.api.repository.model.ISessionRepository;
import ru.mtumanov.tm.api.service.model.ISessionService;
import ru.mtumanov.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

}
